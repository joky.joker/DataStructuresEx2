#include <iostream>
#include "23TreeNode.h"


CalendarEvent * Tree23Node::eventAt(time_t time)
{
	//if the time is before the middle Minimum then we should look at the left part
	if (time < m_mid_min) {
		return m_left->eventAt(time);
	}
	// otherwise if its after the right minimum we should look there
	if (hasThreeChildren() && time >= m_right_min) {
		return m_right->eventAt(time);
	}
	//eventually we should look at the middle part
	return m_mid->eventAt(time);
}

CalendarEvent * Tree23Node::eventAfter(time_t time)
{
	//if the time is bigger or equal to the right minimum then we need to go there
	if (hasThreeChildren() && time >= m_right_min) {
		return m_right->eventAfter(time);
	}
	//otherwise, if the time is smaller than mid minimum we rather go to the left
	if (time < m_mid_min) {
		return m_left->eventAfter(time);
	}
	//eventually we would look at the middle
	return m_mid->eventAfter(time);
}

Tree23NodeLeaf * Tree23Node::insert(CalendarEvent * calEvent, CalendarEvent*& returnValue,
	time_t& leftMinimum, time_t& rightMinimum)
{
	if (calEvent == nullptr) {
		returnValue = nullptr;
		return nullptr;
	}
	Tree23NodeLeaf * result = nullptr;
	Tree23Node * node = nullptr;
	//if the end time of the event inserted is before the middle minimum then we insert to the left
	if (calEvent->getEndtime() < m_mid_min) {
		result = m_left->insert(calEvent, returnValue, leftMinimum, rightMinimum);
		if (result == nullptr || returnValue == nullptr) {
			return nullptr; 
		}

		if (!hasThreeChildren()) {
			//simply move the middle to the right and set the middle as the new nodes
			setRight(m_mid, m_mid_min);
			setMiddle(result, rightMinimum);
		} else {
			//add new node in case we have 3 childrens
			node = new Tree23Node(this->m_mid, m_right_min, this->m_right);
			time_t temp = m_mid_min;
			setMiddle(result, rightMinimum);
			rightMinimum = temp;
			setRight();
		}
		
		return node;
	}
	// if the start time of the event is bigger than the right minimum
	if (hasThreeChildren() && calEvent->getStartTime() > m_right_min) {
		//inserting to the right
		result = m_right->insert(calEvent, returnValue, leftMinimum, rightMinimum);
		//propagating and updating the minimum.
		if (leftMinimum != 0) {
			this->m_right_min = leftMinimum;
		}
		leftMinimum = 0;
		if (result == nullptr || returnValue == nullptr) { return nullptr; }
		//we have 3 children - we need to create another sibling
		node = new Tree23Node(this->m_right, rightMinimum, result);
		rightMinimum = m_right_min;
		//zero out the right child
		setRight();
		return node;
	}
	//if we need to push it into the middle one
	if ((!hasThreeChildren() && calEvent->getStartTime() > m_mid_min) ||
		(hasThreeChildren() && calEvent->getStartTime() > m_mid_min && calEvent->getEndtime() < m_right_min)) {
		result = m_mid->insert(calEvent, returnValue, leftMinimum, rightMinimum);
		//propagating and updating the minimum.
		if (leftMinimum != 0) {
			this->m_mid_min = leftMinimum;
		}
		leftMinimum = 0;
		if (result == nullptr || returnValue == nullptr) return nullptr;
		//if we have an extra space, just set it
		if (!hasThreeChildren()) {
			setRight(result, rightMinimum);
			return nullptr;
		}
		//create a new node
		Tree23Node * node = new Tree23Node(result, this->m_right_min, this->m_right);
		setRight();
		return node;
	}
	//if we passed all of these, then the event probably is equal to one of the minimums
	//therefore we fail
	returnValue = nullptr;
	return nullptr;
}

CalendarEvent * Tree23Node::deleteFirst(Tree23NodeLeaf * rightSibling, time_t& siblingMinimum, bool & isSiblingNeedToBeRemoved)
{
	isSiblingNeedToBeRemoved = false;
	//calling recursively to the left until we get to a leaf
	CalendarEvent * returnValue = m_left->deleteFirst(m_mid, m_mid_min, isSiblingNeedToBeRemoved);
	//if we dont need to remove anything
	if (!isSiblingNeedToBeRemoved) {
		return returnValue;
	}
	//we need to remove the middle sibling
	isSiblingNeedToBeRemoved = false;
	// we have 3 children
	// note that in case this is the root, it will might cause an invalid node with only 1 child
	// we take care of it in the Tree class.
	m_mid = nullptr;
	if (hasThreeChildren() || rightSibling == nullptr) {
		setMiddle(m_right, m_right_min);
		setRight();
		return returnValue;
	}
	Tree23Node * rightNodeSibling = dynamic_cast<Tree23Node *>(rightSibling);
	// we have 2 children and need to delete our middle child, now it depends on our right sibling.
	if (rightNodeSibling->hasThreeChildren()) {
		// we shift a child from the right sibling and update the minimum
		setMiddle(rightNodeSibling->m_left, siblingMinimum);
		rightNodeSibling->setLeft(rightNodeSibling->m_mid);
		siblingMinimum = rightNodeSibling->m_mid_min;
		rightNodeSibling->setMiddle(rightNodeSibling->m_right, rightNodeSibling->m_right_min);
		rightNodeSibling->setRight();
		return returnValue;
	}
	//otherwise our sibling has 2 children, we take its children (now we have 3) and then delete it
	setMiddle(rightNodeSibling->m_left, siblingMinimum);
	setRight(rightNodeSibling->m_mid, rightNodeSibling->m_mid_min);
	isSiblingNeedToBeRemoved = true;
	delete rightSibling;
	return returnValue;
}

void Tree23Node::printInorder()
{
	m_left->printInorder();
	m_mid->printInorder();
	if (hasThreeChildren()) {
		m_right->printInorder();
	}
}




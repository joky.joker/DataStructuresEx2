#pragma once
#include <climits>
#include <ctime>
#include "CalendarEvent.h"
#include "Tree23NodeLeaf.h"

class Tree23Node : public Tree23NodeLeaf {
	Tree23NodeLeaf * m_left = nullptr; //left child
	Tree23NodeLeaf * m_mid = nullptr; //middle child
	Tree23NodeLeaf * m_right = nullptr; //right child
	time_t m_mid_min = 0; // middle child minimum
	time_t m_right_min = 0;// right child minimum
	//set middle child and minimum
	void setMiddle(Tree23NodeLeaf * mid = nullptr, time_t minimum = 0) { m_mid = mid; m_mid_min = minimum; };
	//set right child and minimum
	void setRight(Tree23NodeLeaf * right = nullptr, time_t minimum = 0) { m_right = right; m_right_min = minimum; };
	//set left child
	void setLeft(Tree23NodeLeaf * left = nullptr) { m_left = left; }

public:
	// is this a leaf or a node?
	bool isLeaf() { return false; }
	Tree23Node(Tree23NodeLeaf * left, time_t midMinimum, Tree23NodeLeaf * mid,
		time_t rightMinimum = 0, Tree23NodeLeaf * right = nullptr) :
		m_left(left), m_mid(mid), m_right(right), m_mid_min(midMinimum), m_right_min(rightMinimum) {};
	/*Insert a new event
		Input: 
			calEvent - the event to add
			returnValue - the value to return (null if was unsuccessful and calEvent otherwise)
			leftMinimum - the new minimum of the node called
			rightMinimum - the new Minimum of the new node returned
		Output:
			nullptr if a new node is not needed.
			otherwise a new Node to add to our RIGHT.
	*/
	Tree23NodeLeaf * insert(CalendarEvent * calEvent, CalendarEvent*& returnValue,
		time_t& leftMinimum, time_t& rightMinimum);
	/*
		Deletes the first event int the tree
		Input:
			sibling - our right sibling
			siblingMinimum - the minimum of our right sibling. used as input and output.
			isSiblingNeedToBeRemoved - used to indicate the called whether the sibling is not needed
		Output:
			the event removed from the tree
	*/
	CalendarEvent * deleteFirst(Tree23NodeLeaf * sibling, time_t& siblingMinimum, bool& isSiblingNeedToBeRemoved);
	//Get the event starting at the specified time
	CalendarEvent * eventAt(time_t time);
	//Get the event starting at or after the specified time
	CalendarEvent * eventAfter(time_t time);
	//print the subtree recursively
	void printInorder();
	//check if we have 3 children (or 2)
	bool hasThreeChildren() { return m_right != nullptr; }
	//get middle child
	Tree23NodeLeaf * getMiddle() { return m_mid; }
	//get left child
	Tree23NodeLeaf * getLeft() { return m_left; }

	friend class Tree23Leaf;
};


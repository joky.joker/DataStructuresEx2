#include "TreeNode.h"
#include "BinTree.h"


TreeNode * getParent(TreeNode * root, TreeNode * nodeParent)
{
	if(root == nullptr || nodeParent == root) {
		return nullptr;
	}
	if(root->left == nodeParent) {
		return root;
	}
	if(root->right == nodeParent) {
		return root;
	}
	TreeNode * result = getParent(root->right, nodeParent);
	if(result != nullptr) { return result; }
	result = getParent(root->left, nodeParent);
	if(result != nullptr) { return result; }
	return nullptr;
}

void replaceNodeChild(TreeNode * parent, TreeNode * currentChild, TreeNode * newChild)
{
	if (parent == nullptr) {
		return;
	}
	if (parent->left == currentChild) {
		parent->left = newChild;
	}
	else if (parent->right == currentChild) {
		parent->right = newChild;
	}
}

void replaceOrNullOutParentChild(TreeNode * node1, TreeNode* & nodeptr1, TreeNode * node2, TreeNode* & nodeptr2)
{
	// nodeptr1 should be node1->right or node1->left
	// ex: nodeptr1 = node1->left
	//	   nodeptr2 = node2->left

	//if there are not parent-child
	if (node1 != nodeptr2 && node2 != nodeptr1) {
		std::swap(nodeptr1, nodeptr2); //might not work - not sure.
		return;
	}
	TreeNode * temp = nodeptr1;
	if (node1 != nodeptr2) {
		nodeptr1 = nodeptr2;
		nodeptr2 = node1;
	}
	else {
		nodeptr2 = nodeptr1;
		nodeptr1 = node2;
	}
}

void replaceNodes(BinTree * tree, TreeNode * node1, TreeNode * node2)
{
	TreeNode * parent1 = getParent(tree->root, node1);
	TreeNode * parent2 = getParent(tree->root, node2);

	// check parent1 is not setting himself as his child
	if(parent1 != nullptr && parent1 != node2) {
		replaceNodeChild(parent1, node1, node2);
	}
	//node2 has a parent
	if(parent2 != nullptr && parent2 != node1) {
		replaceNodeChild(parent2, node2, node1);
	}
	//switch childs
	replaceOrNullOutParentChild(node1, node1->right, node2, node2->right);
	replaceOrNullOutParentChild(node1, node1->left, node2, node2->left);
	//Fix tree root.
	if (tree->root == node1) {
		tree->root = node2;
	}
	else if (tree->root == node2) {
		tree->root = node1;
	}
}

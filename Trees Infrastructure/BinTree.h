#pragma once

#include "TreeNode.h"

struct BinTree {
	TreeNode * root = nullptr;

	BinTree(TreeNode* m): root(m) {};
	~BinTree(){
		delete root;
		root = nullptr;
	};
	friend TreeNode * getParent(TreeNode * root, TreeNode * nodeParent);
	friend void replaceNodeChild(TreeNode * parent, TreeNode * currentChild, TreeNode * newChild);
	friend void replaceOrNullOutParentChild(TreeNode * node1, TreeNode* & nodeptr1, TreeNode * node2, TreeNode* & nodeptr2);
	friend void replaceNodes(BinTree * tree, TreeNode * node1, TreeNode * node2);
};
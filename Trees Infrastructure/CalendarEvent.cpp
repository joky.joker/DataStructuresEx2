#include "CalendarEvent.h"
#include <iostream>

#define MAX(a,b) ((a)>(b) ? (a) : (b))

//this method prints out the event
void CalendarEvent::print()
{
	cout << this->startTime << " " << this->duration << this->description << endl;
}

//Does the two events overlap
bool CalendarEvent::isOverlapping(CalendarEvent * calEvent)
{
	//if the other is null then we are cool
	if (calEvent == nullptr) {
		return false;
	}
	//compute the end time of the other event
	time_t otherEndTime = calEvent->duration + calEvent->startTime;
	//if the time it takes from the moment we start one till we finish the other
	// is less then the sum of their duration then they overlap.
	time_t duration1 = abs(otherEndTime - this->startTime);
	time_t duration2 = abs(calEvent->startTime - this->getEndtime());
	return MAX(duration1, duration2) < (this->duration + calEvent->duration);
}

//Is this event occurs before the given event
bool CalendarEvent::operator<(const CalendarEvent & calEvent)
{
	//check if the this event occurs before the next event 
	// by making sure our end time is less or equal then the other's.
	time_t endTime = this->startTime + this->duration;
	return endTime <= calEvent.startTime;
}

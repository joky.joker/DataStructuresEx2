#pragma once
#include <string>
#include <ctime>

using namespace std;

typedef time_t cal_time_t;
//Calendar Event is a class representing an Event in calendar.
class CalendarEvent {
	time_t startTime; // time_t is defined in the header file <ctime>
	time_t duration; // duration of the event, in seconds
	string description; // should not contain special characters or newline
public:
	// constructor, destructor, other methods as needed
	CalendarEvent(time_t start_time, time_t dur, string& desc) :
		startTime(start_time), duration(dur), description(desc) {};
	CalendarEvent(int start_time, int dur, const char desc[]) :
		startTime(start_time), duration(dur), description(desc) {};
	void print(); // "print" the event to cout 
	//check if two events overlap in time
	bool isOverlapping(CalendarEvent * calEvent);
	//check if event occurs before other event.
	bool operator <(const CalendarEvent& calEvent);
	//getters and setters as needed
	time_t getStartTime() { return startTime; }
	time_t getDuration() { return duration; }
	time_t getEndtime() { return startTime + duration; }
	//Check if the event is happening at the specific time
	bool isHappeningInTime(time_t time) { return time < getEndtime() && time >= startTime; }
};
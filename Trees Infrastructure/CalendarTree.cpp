#include <iostream>
#include "CalendarTree.h"
#include "Tree23Leaf.h"
#include "TreeNode.h"
#include "Tree23NodeLeaf.h"

//Insert an event, if it fails we return nullptr
CalendarEvent * CalendarTree::insert(CalendarEvent * calEvent)
{
	if (calEvent == nullptr) {
		return nullptr;
	}
	//first insert
	if (isEmptyTree()) {
		m_first_data = calEvent;
		return calEvent;
	}
	//second insert now we can create a leaf because we have 2 events
	if (isOneData()) {
		// make sure they dont overlap
		if (m_first_data == calEvent || m_first_data->isOverlapping(calEvent)) {
			return nullptr;
		}
		//create a leaf in the right order
		if (m_first_data->getEndtime() < calEvent->getStartTime()) {
			this->root = new Tree23Leaf(m_first_data, calEvent);
		}
		else {
			root = new Tree23Leaf(calEvent, m_first_data);
		}	
		m_first_data = nullptr;
		return calEvent;
	}
	//else we have some kind of tree (either a leaf or a node)
	CalendarEvent * returnValue = nullptr;
	time_t leftMinimum = 0;
	time_t rightMinimum = 0;
	Tree23NodeLeaf * secondLeaf = root->insert(calEvent, returnValue, leftMinimum, rightMinimum);
	if (returnValue == nullptr) {
		return nullptr;
	}
	if (secondLeaf == nullptr) {
		return calEvent;
	}
	// we need to add the secondLeaf to our right, therefore we will create a new node and get them both under it.
	root = new Tree23Node(root, rightMinimum, secondLeaf);
	//indicate success
	return calEvent;
}

CalendarEvent * CalendarTree::eventAt(cal_time_t evtime)
{
	//if we have no events
	if (isEmptyTree()) {
		return nullptr;
	}
	//if we have 1 event and its on the time given
	if (isOneData() && m_first_data->isHappeningInTime(evtime)) {
		return m_first_data;
	}
	//otherwise we will call the tree itself
	return root->eventAt(evtime);
}

CalendarEvent * CalendarTree::eventAfter(cal_time_t evtime)
{
	//if we have an empty tree
	if (isEmptyTree()) {
		return nullptr;
	}
	//if we have 1 event and its after the time given
	if (isOneData() && m_first_data->getStartTime() >= evtime) {
		return m_first_data;
	}
	// we will call the tree search.
	return root->eventAfter(evtime);
}

CalendarTree::~CalendarTree()
{
	delete root;
	root = nullptr;
}

CalendarEvent * CalendarTree::deleteFirst()
{
	CalendarEvent * result = nullptr;
	if (isEmptyTree()) {
		return nullptr;
	}
	if (isOneData()) {
		result = m_first_data;
		m_first_data = nullptr;
		return result;
	}
	// we have a leaf with 2 children
	//these are not relevant as we don't have any siblings
	time_t minimum;
	bool isNeedToBeRemoved = false;
	// one call to deleteFirst will cause a situation of leaf with only 1 child
	// we fix that by making a second call.
	if (isHavingLeaf() && !root->hasThreeChildren()) {
		result = root->deleteFirst(nullptr, minimum, isNeedToBeRemoved);
		m_first_data = root->deleteFirst(nullptr, minimum, isNeedToBeRemoved);
		delete root;
		root = nullptr;
		return result;
	}
	// otherwise if we have leaf with 3 children
	if (isHavingLeaf()) {
		return root->deleteFirst(nullptr, minimum, isNeedToBeRemoved);
	}
	//otherwise we have a tree
	//if we have 3 child root
	if (root->hasThreeChildren()) {
		return root->deleteFirst(nullptr, minimum, isNeedToBeRemoved);
	}
	//otherwise we have 2 children
	// calling deleteFirst will cause a situation of invalid root with 1 child, 
	// we will therefore replace the root with a second deleteFirst call
	result = root->deleteFirst(nullptr, minimum, isNeedToBeRemoved);
	// check if we invalidated the root
	Tree23Node * oldRoot = dynamic_cast<Tree23Node *>(root);
	if (oldRoot->getMiddle() != nullptr) {
		return result;
	}
	// we invalidated the tree, it has only left, and therefore we will set that left as our new root.
	root = oldRoot->getLeft();
	delete oldRoot;
	return result;
}

// we will print all the data in the leafs
void CalendarTree::printSorted()
{
	// in case its a one data tree
	if (isOneData()) {
		m_first_data->print();
		return;
	}
	//if its not a tree for some reason
	if (root == nullptr) {
		return;
	}
	Tree23NodeLeaf * ptr = root;
	//we keep going left until we reach a leaf - log(n) operations
	while (!ptr->isLeaf()) {
		ptr = dynamic_cast<Tree23Node *>(ptr)->getLeft();
	}
	//we reached a leaf - now we will print all the leafs using the linked list we have - O(n)
	Tree23Leaf * leaf = dynamic_cast<Tree23Leaf *>(ptr);
	while (leaf != nullptr) {
		leaf->printInorder();
		leaf = leaf->getNext();
	}
	std::cout << std::endl;
}

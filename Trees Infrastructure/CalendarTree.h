#pragma once
#include "23TreeNode.h"
#include "CalendarEvent.h"

//Calendar Tree class which is basically a 2-3 Tree with minor changes
class CalendarTree {
	Tree23NodeLeaf * root = nullptr; // The root of the Tree
	CalendarEvent * m_first_data = nullptr; // the first data we have, because a tree with one event is not a valid 2-3 Tree.
public:
	CalendarTree() = default;
	~CalendarTree();
	//Adds an event, on success returns the event ptr, on failure returns null_ptr.
	// fails if we have an overlapping event
	CalendarEvent * insert(CalendarEvent * calEvent);
	//Searches for an event at a specific time, returns the ptr on success, on failure returns null.
	CalendarEvent * eventAt(cal_time_t evtime);
	//Searches for an event after a specific time, returns the ptr on success, on failure returns null.
	CalendarEvent * eventAfter(cal_time_t evtime);
	//removes the first event in the tree returning its ptr, but not deleting it from the heap
	CalendarEvent * deleteFirst();
	//prints all event in sorted order
	void printSorted();
	//helper functions for stages
	//First action, we have no data at all
	bool isEmptyTree() { return root == nullptr && this->m_first_data == nullptr;	}
	//Second action, we have have data in our temp ptr, but no tree
	bool isOneData() { return root == nullptr && this->m_first_data != nullptr; }
	//Forth action, we have a leaf only
	bool isHavingLeaf() { return root != nullptr && root->isLeaf(); }
};
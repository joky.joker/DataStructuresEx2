#include "Tree23Leaf.h"
#include <iostream>
using namespace std;


//returning a node to add to our right
Tree23NodeLeaf * Tree23Leaf::insert(CalendarEvent * calEvent, CalendarEvent*& returnValue,
	time_t& leftMinimum, time_t& rightMinimum)
{
	returnValue = nullptr;
	if (calEvent == nullptr) {
		return nullptr;
	}
	//if it overlaps with one of the events then we return nullptr
	if (calEvent->isOverlapping(m_left) || calEvent->isOverlapping(m_mid) || calEvent->isOverlapping(m_right)) {
		return nullptr;
	}
	returnValue = calEvent;
	Tree23Leaf * result = nullptr;
	//if the event occurs before the left one
	if (*calEvent < *m_left) {
		//simply set it, or create a new node to add to our right
		if (!hasThreeChildren()) {
			this->m_right = this->m_mid;
		} else {
			//create a new node and set it in the linked list we form.
			result = new Tree23Leaf(this->m_mid, this->m_right);
			insertAfter(result);
			this->m_right = nullptr;
			//update the the new node minimum
			rightMinimum = this->m_mid->getStartTime();
		}
		this->m_mid = this->m_left;
		this->m_left = calEvent;
		leftMinimum = this->m_left->getStartTime();
		return result;
	}
	//if the event happens after the right event - we need to create a new node
	if (hasThreeChildren() && *this->m_right < *calEvent) {
		result = new Tree23Leaf(this->m_right, calEvent);
		//create a new node and update the minimums
		insertAfter(result);
		rightMinimum = this->m_right->getStartTime();
		leftMinimum = this->m_left->getStartTime();
		this->m_right = nullptr;
		return result;
	}
	//if it happens between the left and the middle
	if (*this->m_left < *calEvent && *calEvent < *this->m_mid) {
		if (!hasThreeChildren()) {
			//we move the middle to the right
			this->m_right = this->m_mid;
		} else {
			//create a new node and shift the middle and the right to it
			result = new Tree23Leaf(this->m_mid, this->m_right);
			insertAfter(result);
			rightMinimum = this->m_mid->getStartTime();
			this->m_right = nullptr;
		}
		this->m_mid = calEvent;
		leftMinimum = m_left->getStartTime();
		return result;
	}
	//if it happens after the middle
	if (*this->m_mid < *calEvent) {
		leftMinimum = m_left->getStartTime();
		//if we have 3 children then we need to make sure it happens before the right
		if (hasThreeChildren() && *calEvent < *this->m_right) {
			result = new Tree23Leaf(calEvent, this->m_right);
			insertAfter(result);
			rightMinimum = calEvent->getStartTime();
			this->m_right = nullptr;
			return result;
		}
		this->m_right = calEvent;
		return result;
	}
	//we should never ever get to this point.
	// we checked for overlaps and for all the possible postions of the new event
	cerr << "SHOULD NEVER HAPPEN" << endl;
	throw std::bad_exception();
	returnValue = nullptr;
	return nullptr;
}

CalendarEvent * Tree23Leaf::deleteFirst(Tree23NodeLeaf * rightSibling, time_t& siblingMinimum, bool& isSiblingNeedToBeRemoved)
{
	isSiblingNeedToBeRemoved = false;
	CalendarEvent * returnValue = m_left;
	m_left = m_mid;
	//if we have 3 children, or we don't have a sibling for some reason - simply remove
	//note that if we have an invalid rightSibling we might create an invalid node with only 1 child
	//we take care of that in the Tree class.
	if (hasThreeChildren() || rightSibling == nullptr) {
		m_mid = m_right;
		m_right = nullptr;
		return returnValue;
	}
	Tree23Leaf * rightLeafSibling = dynamic_cast<Tree23Leaf *>(rightSibling);
	// we have 2 children, he has 3 - we need to split up with our right sibling
	m_mid = rightLeafSibling->m_left;
	//take a child from our sibling and update the minimum
	if (rightLeafSibling->hasThreeChildren()) {
		rightLeafSibling->m_left = rightLeafSibling->m_mid;
		siblingMinimum = rightLeafSibling->m_left->getStartTime();
		rightLeafSibling->m_mid = rightLeafSibling->m_right;
		rightLeafSibling->m_right = nullptr;
		return returnValue;
	}
	//otherwise our sibling has only 2 children, we need to union the children to us, and tell our parent.
	m_right = rightLeafSibling->m_mid;
	isSiblingNeedToBeRemoved = true;
	m_next = rightLeafSibling->m_next;
	delete rightSibling;
	return returnValue;
}

CalendarEvent * Tree23Leaf::eventAt(time_t time)
{
	//check if one of our childrens happen at the specific time above
	if (m_left->isHappeningInTime(time)) {
		return m_left;
	}
	if (m_mid->isHappeningInTime(time)) {
		return m_mid;
	}
	if (hasThreeChildren() && m_right->isHappeningInTime(time)) {
		return m_right;
	}
	return nullptr;
}

CalendarEvent * Tree23Leaf::eventAfter(time_t time)
{
	//if the left happens after the time given
	if (m_left->getStartTime() >= time) {
		return m_left;
	}
	//otherwise, if the middle happens after time given
	if (m_mid->getStartTime() >= time) {
		return m_mid;
	}
	//otherwise if the right
	if (hasThreeChildren() && m_right->getStartTime() >= time) {
		return m_right;
	}
	//if all of it didn't work the we need to take the next event
	return m_next->m_left;
}

void Tree23Leaf::printInorder()
{
	m_left->print();
	m_mid->print();
	if (hasThreeChildren()) {
		m_right->print();
	}
}
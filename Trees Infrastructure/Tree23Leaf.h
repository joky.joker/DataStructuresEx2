#pragma once
#include "CalendarEvent.h"
#include "Tree23NodeLeaf.h"

class Tree23Leaf : public Tree23NodeLeaf {
	Tree23Leaf * m_next = nullptr;     //The next leaf in the linked list of data
	CalendarEvent * m_left = nullptr;  // left child
	CalendarEvent * m_mid = nullptr;   // mid child
	CalendarEvent * m_right = nullptr; // right child
public:
	// is this a leaf or a node?
	bool isLeaf() { return true; }
	Tree23Leaf(CalendarEvent * left, CalendarEvent * mid, CalendarEvent * right = 0) :
		m_left(left), m_mid(mid), m_right(right) {}
	/*Insert a new event
		Input: 
			calEvent - the event to add
			returnValue - the value to return (null if was unsuccessful and calEvent otherwise)
			leftMinimum - the new minimum of the node called
			rightMinimum - the new Minimum of the new node returned
		Output:
			nullptr if a new node is not needed.
			otherwise a new Node to add to our RIGHT.
	*/
	Tree23NodeLeaf * insert(CalendarEvent * calEvent, CalendarEvent*& returnValue,
		time_t& leftMinimum, time_t& rightMinimum);
	/*
		Deletes the first event int the tree
		Input:
			sibling - our right sibling
			siblingMinimum - the minimum of our right sibling. used as input and output.
			isSiblingNeedToBeRemoved - used to indicate the called whether the sibling is not needed
		Output:
			the event removed from the tree
	*/
	CalendarEvent * deleteFirst(Tree23NodeLeaf * sibling, time_t& siblingMinimum, bool& isSiblingNeedToBeRemoved);
	//Get the event starting at the specified time
	CalendarEvent * eventAt(time_t time);
	//Get the event starting at or after the specified time
	CalendarEvent * eventAfter(time_t time);
	//Insert a leaf after us in the linked list
	void insertAfter(Tree23Leaf * next) { next->m_next = this->m_next; this->m_next = next; }
	//Print the leaf data
	void printInorder();
	//Check if we have 3 children (or 2)
	bool hasThreeChildren() { return m_right != nullptr; }
	//Get next Leaf in the linked list
	Tree23Leaf * getNext() { return m_next; }

	friend class Tree23Node;
};

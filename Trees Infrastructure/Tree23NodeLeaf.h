#pragma once
#include "CalendarEvent.h"

//Parent class as Interface for both the leafs and the nodes in the Tree
class Tree23NodeLeaf {
public:
	// is this a leaf or a node?
	virtual bool isLeaf() = 0;
	//Get the event starting at the specified time
	virtual CalendarEvent * eventAt(time_t time) = 0;
	//Get the event starting at or after the specified time
	virtual CalendarEvent * eventAfter(time_t time) = 0;
	/*Insert a new event
		Input: 
			calEvent - the event to add
			returnValue - the value to return (null if was unsuccessful and calEvent otherwise)
			leftMinimum - the new minimum of the node called
			rightMinimum - the new Minimum of the new node returned
		Output:
			nullptr if a new node is not needed.
			otherwise a new Node to add to our RIGHT.
	*/
	virtual Tree23NodeLeaf * insert(CalendarEvent * calEvent, CalendarEvent*& returnValue,
		time_t& leftMinimum, time_t& rightMinimum) = 0;
	/*
		Deletes the first event int the tree
		Input:
			sibling - our right sibling
			siblingMinimum - the minimum of our right sibling. used as input and output.
			isSiblingNeedToBeRemoved - used to indicate the called whether the sibling is not needed
		Output:
			the event removed from the tree
	*/
	virtual CalendarEvent * deleteFirst(Tree23NodeLeaf * sibling, time_t& siblingMinimum, bool& isSiblingNeedToBeRemoved) = 0;
	//print the tree inorder
	virtual void printInorder() = 0;
	//check whether we have 3 children (otherwise we have 2)
	virtual bool hasThreeChildren() = 0;
};
#include <iostream>
#include "TreeNode.h"


using namespace std;

TreeNode::TreeNode()
{
	left = right = nullptr;
	data = 0;
}

TreeNode::TreeNode(int data, TreeNode *left, TreeNode *right)
{
	this->data = data;
	this->left = left;
	this->right = right;
}

void TreeNode::InOrder()
{
	if (left)
		left->InOrder();
	
	cout << data << ", ";

	if (right)
		right->InOrder();
}

TreeNode::TreeNode(int _data, int left, int right) {
	this->data = _data;
	this->left = new TreeNode(left);
	this->right = new TreeNode(right);
}
//Destructors free memory recursively
TreeNode::~TreeNode()
{
	if (left)
		delete left;
	if (right)
		delete right;
}